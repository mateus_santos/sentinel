<?php

/**
 * Sites filter form base class.
 *
 * @package    NOME_DO_PROJETO
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSitesFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'url'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'arquivo' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'url'     => new sfValidatorPass(array('required' => false)),
      'arquivo' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sites_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Sites';
  }

  public function getFields()
  {
    return array(
      'id'      => 'Number',
      'url'     => 'Text',
      'arquivo' => 'Text',
    );
  }
}
