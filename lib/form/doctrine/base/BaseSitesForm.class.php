<?php

/**
 * Sites form base class.
 *
 * @method Sites getObject() Returns the current form's model object
 *
 * @package    NOME_DO_PROJETO
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSitesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'      => new sfWidgetFormInputHidden(),
      'url'     => new sfWidgetFormTextarea(),
      'arquivo' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'url'     => new sfValidatorString(),
      'arquivo' => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('sites[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Sites';
  }

}
