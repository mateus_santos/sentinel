# Sentinel - Killing machine designed for one thing: Search and destroy #

### Objetivo ###

 - Esse aplicativo possui o objetivo de varrer e analisar os links e imagens quebradas contidos no html de determinados sites escolhido pelo usuário. Além de verificar se existem textos esquecidos na página, como o famoso 'Lorem Ipsum';

### Tecnologias utilizadas ###
  - PHP ( Symfony Framework, class mPDF )
  - Html ( Bootstrap )
  - Javascript ( JQuery )
   
### Etapas do desenvolvimento e problemas encontrados ###

  - Primeiramente foi instalado o framework PHP Symfony, para utilização como base para o desenvolvimento da aplicação. 
  - Após muito esforço para realizar a instalação, o entendimento da estrutura e o funcionamento do frame, foi iniciado o desenvolvimento da app. Inicialmente a análise para programar a app, foi de realizar tudo por Ajax, se utilizando da biblioteca Jquery para tal, no entanto, foi visto que requisições Ajax não acessam links externos, criando com isso, mias uma dificuldade durante o desenvolvimento desta app.
  - O modo para contornar o problema dos testes de link, foi a utilização do php para verificação desses links. Portanto, foram criados campos input do tipo hidden, para serem submetidos para o controller, e esse, por sua vez, realizar o tratamento dos dados e retornar para view fazer a renderização do resultado;
  - Realizadas as verificações e renderizado os resultados das mesmas, o próximo passo foi gerar um arquivo pdf com esses resultados, com o intuito que o usuário possa salvar as informações retornadas da app. Novamente, foi encontrado uma dificuldade em conseguir realizar a tarefa, devido muito ao pouco conhecimento do framework

### Conclusões ###
  
   - Portanto, vejo que o desenvolvimento dessa app, foi um desafio que me proporcionou a aquisição de diversos conhecimentos, pois foi a primeira vez que utilizei o framework Symfony, no qual tive grandes problemas para instalar, pois utilizo a plataforma windows como meu ambiente de trabalho,  e enteder seu funcionamento, visto que é bem diferente dos frameworks que já são de meu conhecimento. Além disso, vejo que algumas das minhas decisões para resolver alguns problemas na programação da app, não foram as melhores, devido a dificuldade em realizar alguns debugs no framework.