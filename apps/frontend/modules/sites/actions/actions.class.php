<?php

/**
 * sites actions.
 *
 * @package    sentinel
 * @subpackage sites
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sitesActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    //echo "testeAqui";
	/*$this->sites = Doctrine_Core::getTable('sites')
      ->createQuery('a')
      ->execute();*/
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->sites = Doctrine_Core::getTable('sites')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->sites);
  }

  /*
  *
  *	Controller responsável por renderizar a página principal da app e 
  * realizar todos os tratamentos necessários para testar os links e 
  * imagens quebradas e retornar para a mesma view
  *
  */
  public function executeNew(sfWebRequest $request)
  {
	
	//	Realiza as verificações da app, caso tenha sido	requisitado através de um post
	if ($request->isMethod('post')) 
	{
		
		$return = $request->getPostParameters();
		
		$viewImgs  = "";
		$countLinks = 0;
		$i = 0;
		
		//	Bloco responsável por verificar os headers dos links da página, pegando o código http de retorno 
		foreach( $return['links'] as $link )
		{ 
		
			$file_headers = @get_headers($link);
			$errors = explode(" ",$file_headers[0]);
			//	Caso a faixa do código http seja maior que 400, salva num array o código e a url
			if( $errors[1] >= 400 )
			{
				$viewLinks[$i]['value'] = $link;
				$viewLinks[$i]['code']  = $file_headers[0];				
				$i++;
				$countLinks++;
			}
			
		}
		if($countLinks == 0)
		{
			$viewLinks[0]['value'] = "Nenhum problema encontrado";
			$viewLinks[0]['code']  = "OK";
			
		}
		
		$i = 0;
		//	Bloco responsável por verificar os headers dos links das imagens da página, pegando o código http de retorno 
		foreach( $return['imgs'] as $img )
		{
			$file_headers = @get_headers($img);
			$errors = explode(" ",$file_headers[0]);
			//	Caso a faixa do código http seja maior que 400, salva num array o código e a url
			if( $errors[1] >= 400 )
			{
				$viewImgs[$i]['value'] = $img;	
				$viewImgs[$i]['code']  = $file_headers[0];	
				$countImgs++;
				$i++;
				
			}
							
		}		
		if($countImgs == 0)
		{
			$viewImgs[0]['value'] = "Nenhum problema encontrado";
			$viewImgs[0]['code'] = "OK";
		}
			
		if($return['words'] == "Y")
		{
			$words = "Atenção: Existem Palavras de testes (lorem ipsum) esquecidas no corpo do site";
		}
		else
		{
			$words = "";
		}
		
		
	}
	
	// Passa para view new, todas as variáveis necessárias para construir o retorno do dados e finalizar a aplicação
	$this->viewLinks  =	$viewLinks;
	$this->totalLinks = $return['totalLinks'];
	$this->totalImgs  = $return['totalImgs'];
	$this->viewImgs   =	$viewImgs;
	$this->words      =	$words;
	$this->url      =	$return['url'];
		 
    //$this->form = new sitesForm();
	
  }
    /*
	 * Controller responsável por gerar o pdf
	*/ 	
	public function executePdf(sfWebRequest $request)
    {
		//	Desabilito o layout, para aparecer apenas o conteúdo html
		$this->setLayout(false);
		
		//	Pego todas as variáveis do tipo get
		$return = $request->getGetParameters();
		
		//	Nesse bloco é realizado o tratamento dos paramêtros do tipo array, enviados para esse controller
		$viewLinks = urldecode($return['viewLinks']);
		$viewLinks = stripslashes($viewLinks);
		$viewLinks = unserialize($viewLinks);		
		$viewImgs = urldecode($return['viewImgs']);
		$viewImgs = stripslashes($viewImgs);
		$viewImgs = unserialize($viewImgs);
								
	//	Nesse bloco é realizado a geração do html que será renderizado para o pdf através da classe 'MPDF'
	if(isset($viewLinks) && isset($viewImgs) ){
		
		$html = '
			<div id="img">
				<img src="/images/logo.png" alt="Sentinel" width="150" height="150"/><h2 style="float:left !important;">Url verificada: '.$return['url'].'</h2>
			</div>
			<table class="table table-striped">
			<tr>
				<td><span class="right" style="float: right; !important;"> Total de Links Verificados: '.$return['totalLinks'].'</span></td>
			</tr>
			<tr>
				<td class="active"> Links com Problemas:  </td>
			</tr>';
		
		foreach($viewLinks as $link){
			$html.='
			<tr>
				<td class="info">'.$link['value'].' - <b>'.$link['code'].'</b></td>
			</tr>';
			
		}
		$html.='</table>
		<table class="table table-striped">
		<tr>
			<td><span class="right" style="float: right;"> Total de Imagens Verificadas: '.$return['totalImgs'].'</span></td>
		</tr>
		<tr>
			<td class="active"> Imagens com Problemas: </td>
		</tr>';
		
		
		foreach($viewImgs as $imgs){
			$html.='<tr><td class="info"> '.$imgs['value'].' - <b>'.$imgs['code'].'</b></td></tr>';
			
		}
		$html.="</table>";
		
		if($words != ""){
			$html.='<table class="table table-striped">
				<tr>
					<td class="active">'.$words.'</td>
				</tr>	
			</table>';
		}
	}
		
		// Nesse bloco instancio o objeto da classe mPDF, seto algumas configurações para esse objeto e por fim renderizo o pdf para o arquivo 'download.pdf'
	    $mpdf = new mPDF('en-GB','A4','','',32,25,27,25,16,13);
	    $mpdf->useOnlyCoreFonts = true;	 
	    $stylesheet = file_get_contents(sfConfig::get('sf_web_dir').'/css/mpdfstyletables.css');
	    $mpdf->WriteHTML($stylesheet,1);  // The parameter 1 tells that this is css/style only and no body/html/text
	    $mpdf->WriteHTML($html, 2);
	    $mpdf->Output('download.pdf','D');
		
	}
  
  public function executeCatcher(sfWebRequest $request)
  {
    $this->form = new sitesForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new sitesForm();

    $this->processForm($request, $this->form);
	
	$this->setTemplate('new');
  
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($sites = Doctrine_Core::getTable('sites')->find(array($request->getParameter('id'))), sprintf('Object sites does not exist (%s).', $request->getParameter('id')));
    $this->form = new sitesForm($sites);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($sites = Doctrine_Core::getTable('sites')->find(array($request->getParameter('id'))), sprintf('Object sites does not exist (%s).', $request->getParameter('id')));
    $this->form = new sitesForm($sites);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($sites = Doctrine_Core::getTable('sites')->find(array($request->getParameter('id'))), sprintf('Object sites does not exist (%s).', $request->getParameter('id')));
    $sites->delete();

    $this->redirect('sites/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $sites = $form->save();

     $this->redirect('sites/edit?id='.$sites->getId());
    
	}
  }
}
