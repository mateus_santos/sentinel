<?php if(isset($viewLinks) && isset($viewImgs) ){ 
		
		$encodeLinks = serialize($sf_data->getRaw('viewLinks')); //serializo
        $encodeLinks = urlencode($encodeLinks);
		
		$encodeImgs = serialize($sf_data->getRaw('viewImgs')); //serializo
        $encodeImgs = urlencode($encodeImgs);
		
		
?>
	<a href="<?php echo url_for('sites/pdf')?>?teste=t&viewLinks=<?php echo $encodeLinks; ?>&totalLinks=<?php echo $totalLinks;?>&viewImgs=<?php echo $encodeImgs;?>&totalImgs=<?php echo $totalImgs;?>&words=<?php echo $words;?>&url=<?php echo $url;?>"> <img src="/images/pdf.png" style="width: 36px;height: 36px;margin: 10px 10px;float: right;" /></a>
	<table class="table table-striped">
		<tr>
			<td class="active"> Links com Problemas:  <span class="right"> Total de Links Verificados: <?php echo $totalLinks; ?></span></td>
		</tr>
		<?php 	
		foreach($viewLinks as $link){
		?>
		<tr>
			<td class="info"><a href="<?php echo $link['value'];?>"> <?php echo $link['value']."</a> - <b>".$link['code']."</b>"; ?></td>
		</tr>
<?php } ?>
	</table>
	<table class="table table-striped">
		<tr>
			<td class="active"> Imagens com Problemas: <span class="right"> Total de Imagens Verificadas: <?php echo $totalImgs; ?></span></td>
		</tr>
		<?php 	
		foreach($viewImgs as $imgs)
		{
		?>
		<tr>
			<td class="info"><?php echo $imgs['value']." - <b>".$imgs['code']."</b>"; ?></td>
		</tr>
<?php   } ?>
	</table>
	<?php if($words != ""){ ?>
	<table class="table table-striped">
		<tr>
			<td class="active"> <?php echo $words; ?> </td>
		</tr>	

	</table>
<?php 	} ?>	
<?php
} 
?>