<?php
header('Access-Control-Allow-Origin: *');

?>
<!-- apps/frontend/templates/layout.php -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Sentinel - Killing machine designed for one thing: Search and destroy</title>
    <link rel="shortcut icon" href="/favicon.ico" />
	<link rel="stylesheet" type="text/css" media="screen" href="/css/main.css">
	<link rel="stylesheet" type="text/css" media="screen" href="/css/bootstrap.css">
	<script type="text/javascript" src="/js/jquery-1.12.1.min.js"></script>	
<!-- arquivo javascript foi inserido no site para utilizar para realizar a requisição em ajax, pois no arquivo js chamado não estava funcionando -->
	<script type="text/javascript">
		$(document).ready(function(){
			/*
			* Script responsável por realizar o carregamento (load) do html da página desejada
			* parsear por ele, localizando todas as tags <a> e <img> e as palavras 'lorem ipsum'
			* contido ou não no código.
 			*
			* Problemas encontrados: 
			* 1) Não foi possível inserir esse script em um arquivo js separado, principalmente pela falta
			* de conhecimento no framework symfony 1.4;
			*
			* 2) A idéia inicial era testar os links através de requisições ajax, no entanto descobri
			* não ser possível fazer isso. Portanto, escolhi gerar input's hidden de todos os links e 
			* passar para um script php por post e nesse script, realizo os testes necessários retornando para view
			* os resultados.
			*
			*/
			
			var urlSearch = "";
			$('#search').bind('click', function(){
				urlSearch = $('#url').val();
				
				//	Mostro a div onde está o loader (gif animado)
				$("#loader").show();
				
				//	Verificação da autenticidade da url inserida
				if( urlSearch == "" || urlSearch == undefined || (urlSearch.indexOf("http://") == "-1" && urlSearch.indexOf("https://") == "-1"))
				{
					alert("Insira uma url corretamente. Ex.: http:://www.exemplo.com.br");
					$("#loader").hide();
				}else{
					// Nesse momento realizo o carregamento do html da página requisitada e crio os campos hidden com os links e imagens
					$.ajax({
						url: '<?php echo url_for('sites/index')?>?url='+urlSearch,
						type: 'GET',
						success: function(res) {					
							
							//	Verificação de texto esquecido
							if( res.indexOf("lorem ipsum") != "-1" )
							{
								$("#group-search").append('<input type="hidden" name="words" id="words" value="Y" />');	
								
							}else
							{								
								$("#group-search").append('<input type="hidden" name="words" id="words" value="N" />');	
												
							}
							
							var totalLinks = 0;
							var href="";
							
							// Percorre todos os links (tags <a>) e cria campos input tipo hidden dentro do formulário
							$(res).find('a').each(function()
							{
								
								if($(this).attr('href') != undefined)
								{
									if( $(this).attr('href').indexOf("http://") == "-1" || $(this).attr('href').indexOf("https://") == "-1" )
									{
										var href = urlSearch + $(this).attr('href');
									}else
									{
										var href = $(this).attr('href');
									}

									$("#group-search").append('<input type="hidden" name="links[]" id="links" value="'+href+'" />');			
									
								}
								totalLinks++;
							}); 
							
							//	Envia para o script o número total de Links percorridos
							$("#group-search").append('<input type="hidden" name="totalLinks" id="links" value="'+totalLinks+'" />');			
							
							
							var src="";
							var totalImgs = 0;
							
							// Percorre todas as imagens (tags <img>) e cria campos input tipo hidden dentro do formulario
							$(res).find('img').each(function(){
																
								if( $(this).attr('src').indexOf("http://") == "-1" || $(this).attr('src').indexOf("https://") == "-1" )
								{
									var src = urlSearch + $(this).attr('src');
								}else
								{
									var src = $(this).attr('src');
								}
								$("#group-search").append('<input type="hidden" name="imgs[]" id="imgs" value="'+src+'" />');			
								totalImgs++;
							});
							
							//	Envia para o script o número total de Links percorridos
							$("#group-search").append('<input type="hidden" name="totalImgs" id="totalImgs" value="'+totalImgs+'" />');	
							//	Envia para o script a url verificada, isso será utilizado para geração do pdf
							$("#group-search").append('<input type="hidden" name="url" id="url" value="'+urlSearch+'" />');								
							
							//	Realiza o submit do formulário
							$("#action").submit();
							
						}
					}); 
					
				}
			});			
		});		
	</script>
</head>
  <body>  
    <div id="container">
	<div id="loader"> <img src="/images/loader(1).gif" id="imgLoader"/> </div>
      <div id="header">
        <div class="content">
		  <h1>
			<a href="<?php echo url_for('sites/new') ?>">
				<img src="/images/logo.png" alt="Sentinel" />
			</a>
		  </h1>
 
          <div id="sub_header">
            <div class="search">
              <form action="<?php echo url_for('sites/new') ?>" class="form-inline" method="post" id="action">
				<div class="form-group">
					<label for="exampleInputEmail1">Informe o site:</label>
				</div>		
				<div class="form-group" id="group-search">
					<input class="form-control" placeholder="Insira Url" type="text" name="url" id="url" value="<?php echo $url; ?>" />
					
					<button type="button" class="btn btn-default" id="search" />Enviar</button>
				</div>	           
              </form>
            </div>
          </div>
        </div>
      </div>
 
      <div id="content">
		<div id="return">
			<!-- Conteúdo carregado da view (template) -->
			<?php echo $sf_content; ?>
		</div>
		<div id="loadHtml">
			
		</div>
		<div id="verifyLinksImg">
			
		</div>
      </div>
 
      <div id="footer">
        <div class="content">
          
        </div>
      </div>
	  
    </div>
  </body>
</html>